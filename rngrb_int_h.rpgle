      /if not defined (RNGRB_INT_H)
      /define RNGRB_INT_H
      
      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
     D tree_rb_int_put...
     D                 PR                  extproc('tree_rb_int_put')
     D  tree                           *   const
     D  key                          10I 0 const
     D  value                          *   const options(*string)
     D  length                       10I 0 const options(*nopass)
      *
     D tree_rb_int_get...
     D                 PR              *   extproc('tree_rb_int_get')
     D   tree                          *   const
     D   key                         10I 0 const
      *
     D tree_rb_int_first...
     D                 PR              *   extproc('tree_rb_int_first')
     D   tree                          *   const
      *
     D tree_rb_int_last...
     D                 PR              *   extproc('tree_rb_int_last')
     D   tree                          *   const
      *
     D tree_rb_int_next...
     D                 PR              *   extproc('tree_rb_int_next')
     D   nodePtr                       *   const
      *
     D tree_rb_int_previous...
     D                 PR              *   extproc('tree_rb_int_previous')
     D   nodePtr                       *   const
      *
     D tree_rb_int_containsKey...
     D                 PR              N   extproc('tree_rb_int_containsKey')
     D   tree                          *   const
     D   key                         10I 0 const
      *
     D tree_rb_int_remove...
     D                 PR                  extproc('tree_rb_int_remove')
     D   tree                          *   const
     D   key                         10I 0 const
      *
     D tree_rb_int_removeByNode...
     D                 PR                  extproc('tree_rb_int_removeByNode')
     D   tree                          *   const
     D   node                          * 
      *
     D tree_rb_int_container_of...
     D                 PR              *   extproc('tree_rb_int_container_of')
     D   eNode                         *   value
      *
     D tree_rb_int_compare...
     D                 PR            10I 0 extproc('tree_rb_int_compare')
     D  nodeA                          *   value
     D  nodeB                          *   value
      *
     D tree_rb_int_clear...
     D                 PR                  extproc('tree_rb_int_clear')
     D   tree                          *   const
      *
     D tree_rb_int_finalize...
     D                 PR                  extproc('tree_rb_int_finalize')
     D  tree                           *
      *
      
      
      *-------------------------------------------------------------------------
      * Data Structures
      *-------------------------------------------------------------------------
     D tree_node_int_t...
     D                 DS                  qualified template align
     D  key                          10I 0
     D  value                          *
     D  length                       10I 0
      
      /endif
      