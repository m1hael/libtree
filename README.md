# libtree

This project offers various tree implementations starting with the red black 
tree implementation.


## Base

The base for the project are provided by the [libtree project on github](https://github.com/fbuihuu/libtree) 
which is a C implementation for various tree data structures. The code can be 
compiled on IBM i with the ILE C compiler with almost no changes to the code.


## RPG

This project contains prototypes for the C functions. On top of that the project 
has wrapper modules for the red black tree C implementation to make it easier to 
use the C functions. The wrapper modules support integer and strings as keys. 
The values are added untyped (via pointer).

### String Module

The string module supports strings as keys. The value of the entry can be 
anything as it is passed as a pointer. The module provides two procedures for 
key comparison.

```
tree_rb_string_compare
tree_rb_string_compare_ignore_case
```

As the name already points out, the ignore case variant of the comparison 
procedure evaluates "My Key" and "my key" as equal. 


## Requirements

This software has no further dependencies. It comes with all necessary files.


## Installation
For standard installation the setup script can be executed as is. This will 
build the service program in the library *OSSILE*. If you want to build the
service program in any other library export the library name in the variable
`TARGET_LIB` like this

    export TARGET_LIB=MSCHMIDT

before executing the *setup* script.

For automatically copying the copybook to a directory in the IFS export 
`INCDIR` like this

    export INCDIR=/usr/local/include/

before executing the setup script. The directory which is stated in the export
should exist before executing the script.


## Example

Take a look in the example section to see how to use the procedures.


## Documentation

The documentation for the wrapper modules can be found at the open documentation
library [ILEDocs](http://iledocs.rpgnextgen.com) at [rpgnextgen.com](https://rpgnextgen.com).


## License

This library is licensed under the GNU Lesser General Public License.
