      /if not defined (LIBTREE_H)
      /define LIBTREE_H


      *---------------------------------------------------------------------------------------------
      *
      * (C) Copyleft 2011 Mihael Schmidt
      *
      * This file is part of libtree.
      *
      * libtree is free software: you can redistribute it and/or modify
      * it under the terms of the GNU Lesser General Public License as published
      * by the Free Software Foundation, either version 3 of the License, or
      * any later version.
      *
      * libtree is distributed in the hope that it will be useful,
      * but WITHOUT ANY WARRANTY; without even the implied warranty of
      * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      * GNU Lesser General Public License for more details.
      *
      * You should have received a copy of the GNU Lesser General Public License
      * along with libtree.  If not, see <http://www.gnu.org/licenses/>.
      *
      *---------------------------------------------------------------------------------------------


      *-------------------------------------------------------------------------
      * Data Structures
      *-------------------------------------------------------------------------
       // struct rbtree {
       //   struct rbtree_node *root;
       //   rbtree_cmp_fn_t cmp_fn;
       //   struct rbtree_node *first, *last;
       // };
     D rbtree_t        DS                  qualified template align
     D   root                          *
     D   compare                       *   procptr
     D   first                         *
     D   last                          *

       // struct rbtree_node {
       //   struct rbtree_node *left, *right;
       //   struct rbtree_node *parent;
       //   enum rb_color color;
       // };
     D rbnode_t        DS                  qualified template align
     D   left                          *
     D   right                         *
     D   parent                        *
     D   color                       10I 0


      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D rbtree_init     PR                  extproc('rbtree_init')
     D   tree                          *   value
     D   compare                       *   procptr value
     D   flags                       10U 0 value
      *
     D rbtree_first    PR              *   extproc('rbtree_first')
     D   tree                          *   value
      *
     D rbtree_last     PR              *   extproc('rbtree_last')
     D   tree                          *   value
      *
     D rbtree_next     PR              *   extproc('rbtree_next')
     D   node                          *   value
      *
     D rbtree_previous...
     D                 PR              *   extproc('rbtree_prev')
     D   node                          *   value
      *
     D rbtree_lookup   PR              *   extproc('rbtree_lookup')
     D   key                           *   value
     D   tree                          *   value
      * 
     D rbtree_insert   PR              *   extproc('rbtree_insert')
     D   node                          *   value
     D   tree                          *   value
      *
     D rbtree_remove   PR              *   extproc('rbtree_remove')
     D   node                          *   value
     D   tree                          *   value
      *
     D rbtree_replace  PR              *   extproc('rbtree_replace')
     D   oldNode                       *   value
     D   newNode                       *   value
     D   tree                          *   value

      /include 'rngrb_h.rpgle'
      /include 'rngrb_int_h.rpgle'
      /include 'rngrb_str_h.rpgle'
      
      /endif
