     /**
      * \brief Red Black Tree Implementation : Wrapper
      *
      * This module is a wrapper around the red black tree implemtation
      * from the libtree project at github.com.
      * <p>
      * It also manages the dynamic memory allocation and deallocation.
      * The user can choose to make the allocation from a new or existing
      * user heap. The default heap can also be used.
      * <p>
      * The tree has no maximum number of elements. As long as you can get
      * memory from your heap you can add elements to the tree.
      * <p>
      * This module contains all general procedures for the wrapper which 
      * is not specific for any datatype.
      *
      * \author Mihael Schmidt
      * \date   2011-02-07
      *
      * \link https://github.com/fbuihuu/libtree libtree project
      * \link http://www.rpgnextgen.com RPG Next Gen
      *
      */


     H nomain
      /if defined(THREAD_SAFE)
     H THREAD(*CONCURRENT)
      /endif


      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D countNode       PR
     D   node                          *   const
     D   count                       10I 0

     D removeNodesRecursively...
     D                 PR
     D   tree                          *   const
     D   node                          *

      /include 'libtree_h.rpgle'
      /include 'libc_h.rpgle'


      *-------------------------------------------------------------------------
      * Data Structures
      *-------------------------------------------------------------------------
     D tree_header_t   DS                  qualified template align
     D   tree                          *
     D   heapCreated                   N
     D   deallocate                    N
      *
     D tree_rb_node_int_t...
     D                 DS                  qualified template align
     D  key                          10I 0
     D  value                          *
     D  length                       10I 0
     D  node                               likeds(rbnode_t)
      

      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------

     /**
      * \brief Create tree
      *
      * Creates a new tree. If no id for a heap (user or default heap) is
      * provided a new user heap will be created for memory allocation.
      * <p>
      * A compare function must be passed to the constructor for comparing two
      * nodes. See <code>tree_rb_int_compare</code> for an implementation
      * with integer keys.
      * <p>
      * Per default any memory from removed nodes will be deallocated but you
      * can also passed *on as the third parameter and have the system free all
      * allocated resources on activation group end.
      * 
      * \param Compare function
      * \param Deallocate memory (default: *on)
      *
      * \return Pointer to new tree
      *
      * \info The calling program must make sure to free the memory of the tree
      *       either with the procedure <code>tree_rb_finalize</code> or by
      *       discarding the heap manually. All allocated memory will automatically
      *       be free with the end of the activation group.
      */
     P tree_rb_create  B                   export
     D                 PI              *
     D   compareFunction...
     D                                 *   procptr value
     D   pDeallocate                   N   const options(*nopass)
      *
     D header          DS                  likeds(tree_header_t) based(tree)
     D deallocate      S               N   inz(*on)
      /free
       if (%parms() = 2);
         deallocate = pDeallocate;
       endif;

       tree = %alloc(%size(tree_header_t));
       header.tree = %alloc(%size(rbtree_t));

       rbtree_init(header.tree : compareFunction : 0);
       header.deallocate = deallocate;
       
       return tree;
      /end-free
     P                 E


     /**
      * \brief Is empty
      *
      * Checks if the tree is empty.
      *
      * \param Pointer to tree
      *
      * \return *on = the tree is empty <br>
      *         *off = tree has elements
      */
     P tree_rb_isEmpty...
     P                 B                   export
     D                 PI              N
     D   tree                          *   const
      *
     D header          DS                  likeds(tree_header_t) based(tree)
     D tree_internal   DS                  likeds(rbtree_t) based(header.tree)
      /free
       return (tree_internal.root = *null);
      /end-free
     P                 E


     /**
      * \brief Get tree size
      *
      * Returns the number of elements/nodes in this tree.
      *
      * \param Pointer to tree
      *
      * \return Number of elements in the tree
      *
      * \info As the number of elements is not saved in the head data structure
      *       this procedure must traverse the whole tree before returning the
      *       result. This might take some time on huge trees.
      */
     P tree_rb_size...
     P                 B                   export
     D                 PI            10I 0
     D   tree                          *   const
      *
     D header          DS                  likeds(tree_header_t) based(tree)
     D tree_internal   DS                  likeds(rbtree_t) based(header.tree)
     D nodeCount       S             10I 0
      /free
       if (tree_internal.root <> *null);
         countNode(tree_internal.root : nodeCount);
       endif;

       return nodeCount;
      /end-free
     P                 E


     P countNode       B                   export
     D                 PI                  
     D   nodePtr                       *   const
     D   count                       10I 0
      *
     D node            DS                  likeds(rbnode_t) based(nodePtr)
      /free
       // count this node
       count += 1;

       if (node.left <> *null);
         countNode(node.left : count);
       endif;

       if (node.right <> *null);
         countNode(node.right : count);
       endif;
      /end-free
     P                 E
