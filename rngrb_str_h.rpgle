      /if not defined (RNGRB_STR_H)
      /define RNGRB_STR_H
      
      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
     D tree_rb_string_put...
     D                 PR                  extproc('tree_rb_string_put')
     D   tree                          *   const
     D   key                      65535A   const varying
     D   value                         *   const options(*string)
     D   length                      10I 0 const options(*nopass)
      *
     D tree_rb_string_get...
     D                 PR              *   extproc('tree_rb_string_get')
     D   tree                          *   const
     D   key                      65535A   const varying
      *
     D tree_rb_string_first...
     D                 PR              *   extproc('tree_rb_string_first')
     D   tree                          *   const
      *
     D tree_rb_string_last...
     D                 PR              *   extproc('tree_rb_string_last')
     D   tree                          *   const
      *
     D tree_rb_string_next...
     D                 PR              *   extproc('tree_rb_string_next')
     D   nodePtr                       *   const
      *
     D tree_rb_string_previous...
     D                 PR              *   extproc('tree_rb_string_previous')
     D   nodePtr                       *   const
      *
     D tree_rb_string_containsKey...
     D                 PR              N   extproc('tree_rb_string_containsKey')
     D   tree                          *   const
     D   key                      65535A   const varying
      *
     D tree_rb_string_remove...
     D                 PR                  extproc('tree_rb_string_remove')
     D   tree                          *   const
     D   key                      65535A   const varying
      *
     D tree_rb_string_removeByNode...
     D                 PR                  extproc('tree_rb_string_removeByNode-
     D                                     ')
     D   tree                          *   const
     D   node                          * 
      *
     D tree_rb_string_container_of...
     D                 PR              *   extproc('tree_rb_string_container_of-
     D                                     ')
     D   eNode                         *   value
      *
     D tree_rb_string_compare...
     D                 PR            10I 0 extproc('tree_rb_string_compare')
     D   nodeA                         *   value
     D   nodeB                         *   value
      *
     D tree_rb_string_compare_ignore_case...
     D                 PR            10I 0 extproc('tree_rb_string_compare_+
     D                                              ignore_case')
     D   nodeA                         *   value
     D   nodeB                         *   value
      *
     D tree_rb_string_clear...
     D                 PR                  extproc('tree_rb_string_clear')
     D   tree                          *   const
      *
     D tree_rb_string_finalize...
     D                 PR                  extproc('tree_rb_string_finalize')
     D  tree                           *
     
      
      *-------------------------------------------------------------------------
      * Data Structures
      *-------------------------------------------------------------------------
     D tree_node_string_t...
     D                 DS                  qualified template align
     D  key                            *                 
     D  keyLength                    10I 0
     D  value                          *
     D  length                       10I 0
      
      /endif
      