     /**
      * \brief Red Black Tree Example
      * 
      * This example shows the usage of the LIBTREE service program. 
      * 
      * \author Mihael Schmidt
      * \date   08.02.2011
      */ 


      *-------------------------------------------------------------------------
      * PEP
      *-------------------------------------------------------------------------
     D rb01            PR                  extpgm('RB01')
     D rb01            PI


      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
     D main            PR

      /include 'libtree/libtree_h.rpgle'

      /free
       main();

       *inlr = *on;
       return;
      /end-free


     P main            B
      *
     D tree            S               *
     D nodePtr         S               *
     D node            DS                  likeds(tree_node_int_t)
     D                                     based(nodePtr)
     D                 DS
     D value                        100A
     D val                            5A   overlay(value)
     D dsp             S             50A
      /free
       tree = tree_rb_create(%paddr('tree_rb_int_compare'));

       // fill tree
       tree_rb_int_put(tree : 32425 : 'Minden');
       tree_rb_int_put(tree : 80331 : 'Munich');
       tree_rb_int_put(tree : 30159 : 'Hannover');
       tree_rb_int_put(tree : 10115 : 'Berlin');

       dsply 'Iterate forward ...';
       nodePtr = tree_rb_int_first(tree);
       dow (nodePtr <> *null);
         dsp = %char(node.key) + ' ' + %str(node.value);
         dsply dsp;
         nodePtr = tree_rb_int_next(nodePtr);
       enddo;

       dsply 'Iterate backward ...';
       nodePtr = tree_rb_int_last(tree);
       dow (nodePtr <> *null);
         dsp = %char(node.key) + ' ' + %str(node.value);
         dsply dsp;
         nodePtr = tree_rb_int_previous(nodePtr);
       enddo;

       dsply %trimr('Size: ' + %char(tree_rb_size(tree)));

       dsply 'Search Zipcode/PLZ 10115 ...';
       nodePtr = tree_rb_int_get(tree : 10115);
       if (nodePtr = *null);
         dsply 'not found';
       else;
         dsp = 'Found ' + %str(node.value);
         dsply dsp;
       endif;
       
       dsply 'Clear tree ...';
       tree_rb_int_clear(tree);

       dsply %trimr('Size: ' + %char(tree_rb_size(tree)));

       tree_rb_int_finalize(tree);
      /end-free
     P                 E
