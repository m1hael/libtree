     /**
      * \brief Red Black Tree String Example
      *
      * This example shows the usage of the LIBTREE service program
      * with strings as keys.
      *
      * \author Mihael Schmidt
      * \date   26.03.2011
      */


      *-------------------------------------------------------------------------
      * PEP
      *-------------------------------------------------------------------------
     D rb02            PR                  extpgm('RB02')
     D rb02            PI


      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
     D main            PR

      /include 'libtree/libtree_h.rpgle'

      /free
       main();

       *inlr = *on;
       return;
      /end-free


     P main            B
      *
     D tree            S               *
     D nodePtr         S               *
     D node            DS                  likeds(tree_node_string_t)
     D                                     based(nodePtr)
     D                 DS
     D value                        100A
     D dsp             S             50A
      /free
       tree = tree_rb_create(%paddr('tree_rb_string_compare'));

       // fill tree
       tree_rb_string_put(tree : 'de Lioncourt' : 'Lestat');
       tree_rb_string_put(tree : 'de Pointe de Lac' : 'Louis');
       tree_rb_string_put(tree : 'Blackwood' : 'Tarquin');
       tree_rb_string_put(tree : 'de Lenfent' : 'Nicolas');
       tree_rb_string_put(tree : 'de Romanus' : 'Marius');

       dsply 'Iterate forward ...';
       nodePtr = tree_rb_string_first(tree);
       dow (nodePtr <> *null);
         dsp = %str(node.key) + ' ' + %str(node.value);
         dsply dsp;
         nodePtr = tree_rb_string_next(nodePtr);
       enddo;

       dsply 'Iterate backward ...';
       nodePtr = tree_rb_string_last(tree);
       dow (nodePtr <> *null);
         dsp = %str(node.key) + ' ' + %str(node.value);
         dsply dsp;
         nodePtr = tree_rb_string_previous(nodePtr);
       enddo;

       dsply %trimr('Size: ' + %char(tree_rb_size(tree)));

       dsply 'Search Louis de Pointe de Lac ...';
       nodePtr = tree_rb_string_get(tree : 'de Pointe de Lac');
       if (nodePtr = *null);
         dsply 'not found';
       else;
         dsp = 'Found ' + %str(node.value);
         dsply dsp;
       endif;

       dsply 'Clear tree ...';
       tree_rb_string_clear(tree);

       dsply %trimr('Size: ' + %char(tree_rb_size(tree)));

       tree_rb_string_finalize(tree);
      /end-free
     P                 E
