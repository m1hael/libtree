      /if not defined (RNGRB_H)
      /define RNGRB_H
      
      *-------------------------------------------------------------------------
      * Prototypen
      *-------------------------------------------------------------------------
     D tree_rb_create...
     D                 PR              *   extproc('tree_rb_create')
     D   compareFunction...
     D                                 *   procptr value
     D   deallocate                    N   const options(*nopass)
      *
     D tree_rb_isEmpty...
     D                 PR              N   extproc('tree_rb_isEmpty')
     D   tree                          *   const
      *
     D tree_rb_size...
     D                 PR            10I 0 extproc('tree_rb_size')
     D   tree                          *   const
     
      /endif
      