     /**
      * \brief Red Black Tree Implementation : Wrapper for string keys
      *
      * This module is a wrapper around the red black tree implemtation
      * from the libtree project at github.com.
      * <p>
      * The wrapper adds type specific procedures for string keys.
      * It also manages the dynamic memory allocation and deallocation.
      * Memory will be allocated from the default heap. Depending on the 
      * compile options this is either from single level storage or from 
      * teraspace.
      * <p>
      * The tree has no maximum number of elements. As long as you can get
      * memory from your heap you can add elements to the tree.
      *
      * \author Mihael Schmidt
      * \date   2011-03-08
      *
      * \link https://github.com/fbuihuu/libtree libtree project
      * \link http://www.rpgnextgen.com RPG Next Gen
      *
      */


     H nomain
      /if defined(THREAD_SAFE)
     H THREAD(*CONCURRENT)
      /endif
      
      
      *-------------------------------------------------------------------------
      * Prototypes
      *-------------------------------------------------------------------------
     D countNode       PR
     D   node                          *   const
     D   count                       10I 0

     D removeNodesRecursively...
     D                 PR
     D   tree                          *   const
     D   node                          *

      /include 'libtree_h.rpgle'
      /include 'libc_h.rpgle'


      *-------------------------------------------------------------------------
      * Data Structures
      *-------------------------------------------------------------------------
     D tree_header_t   DS                  qualified template align
     D   tree                          *
     D   deallocate                    N
      *
     D tree_rb_node_string_t...
     D                 DS                  qualified template align
     D  key                            *   
     D  keyLength                    10I 0
     D  value                          *
     D  length                       10I 0
     D  node                               likeds(rbnode_t)
      

      *-------------------------------------------------------------------------
      * Procedures
      *-------------------------------------------------------------------------

     /**
      * \brief Insert new element
      *
      * Inserts a new element into the tree. Any existing element will be
      * replaced. The resources of any existing element will be freed.
      * <p>
      * The value is a passed as a pointer to the real value. If the length
      * parameter is omitted it is assumed that the value is null-terminated.
      * <p>
      * The key and value will be copied to the tree node.
      *
      * \param Pointer to tree
      * \param Key
      * \param Pointer to value
      * \param Value length (default: value must be null-terminated)
      */
     P tree_rb_string_put...
     P                 B                   export
     D                 PI
     D   tree                          *   const
     D   pKey                     65535A   const varying
     D   value                         *   const options(*string)
     D   length                      10I 0 const options(*nopass)
      *
     D key             S          65535A   varying
     D header          DS                  likeds(tree_header_t) based(tree)
     D node            DS                  likeds(tree_rb_node_string_t)
     D                                     based(nodePtr)
     D null            S              1A   inz(x'00')
     D embeddedPtr     S               *
     D userNodePtr     S               *
      /free
       key = pKey;
       
       nodePtr = %alloc(%size(tree_rb_node_string_t));
       node.keyLength = %len(key);
       node.key = %alloc(node.keyLength);
       memcpy(node.key : %addr(key : *DATA) : node.keyLength);

       if (%parms() = 3);
         node.length = strlen(value);
         node.value = %alloc(node.length + 1);
         memcpy(node.value : value : node.length + 1); // also copy the null value at the end
       else;
         node.length = length;
         node.value = %alloc(node.length + 1);
         memcpy(node.value : value : node.length);
         // add null
         memcpy(node.value + node.length : %addr(null) : 1);
       endif;

       embeddedPtr = rbtree_insert(%addr(node.node) : header.tree);
       if (embeddedPtr <> *null); // means the key does already exist in the tree
         // remove existing entry
         userNodePtr = tree_rb_string_container_of(embeddedPtr);
         tree_rb_string_removeByNode(tree : userNodePtr);

         // add new entry
         rbtree_insert(%addr(node.node) : header.tree);
       endif;
      /end-free
     P                 E


     /**
      * \brief Get element
      *
      * Returns a pointer to the user node of the given key.
      *
      * \param Pointer to tree
      * \param Key
      *
      * \return Pointer to user data structure or *null if there is no element
      *         with the given key in the tree.
      */
     P tree_rb_string_get...
     P                 B                   export
     D                 PI              *
     D   tree                          *   const
     D   pKey                     65535A   const varying
      *
     D key             S          65535A   varying
     D header          DS                  likeds(tree_header_t) based(tree)
     D node            DS                  likeds(tree_rb_node_string_t)
     D embeddedPtr     S               *
      /free
       clear node;
       
       key = pKey;
       
       node.keyLength = %len(key);
       node.key = %addr(key : *DATA);
       
       embeddedPtr = rbtree_lookup(%addr(node.node) : header.tree);
       
       return tree_rb_string_container_of(embeddedPtr);
      /end-free
     P                 E


     /**
      * \brief Get first element
      *
      * Returns the first element of the tree.
      *
      * \param Pointer to tree
      *
      * \return Pointer to user data structure or *null if the tree is empty.
      */
     P tree_rb_string_first...
     P                 B                   export
     D                 PI              *
     D   tree                          *   const
      *
     D header          DS                  likeds(tree_header_t) based(tree)
     D embeddedPtr     S               *
      /free
       embeddedPtr = rbtree_first(header.tree);
       return tree_rb_string_container_of(embeddedPtr);
      /end-free
     P                 E


     /**
      * \brief Get last element
      *
      * Returns the last element of the tree.
      *
      * \param Pointer to tree
      *
      * \return Pointer to user data structure or *null if the tree is empty.
      */
     P tree_rb_string_last...
     P                 B                   export
     D                 PI              *
     D   tree                          *   const
      *
     D header          DS                  likeds(tree_header_t) based(tree)
     D embeddedPtr     S               *
      /free
       embeddedPtr = rbtree_last(header.tree);
       return tree_rb_string_container_of(embeddedPtr);
      /end-free
     P                 E


     /**
      * \brief Get next element
      *
      * Returns the next element of the tree starting from the passed element.
      *
      * \param Pointer to user data structure
      *
      * \return Pointer to user data structure of the next node or *null if
      *         there are no more elements after the passed one.
      */
     P tree_rb_string_next...
     P                 B                   export
     D                 PI              *
     D   nodePtr                       *   const
      *
     D node            DS                  likeds(tree_rb_node_string_t)
     D                                     based(nodePtr)
     D embeddedPtr     S               *
      /free
       embeddedPtr = rbtree_next(%addr(node.node));
       return tree_rb_string_container_of(embeddedPtr);
      /end-free
     P                 E


     /**
      * \brief Get previous element
      *
      * Returns the previous element of the tree starting from the passed element.
      *
      * \param Pointer to user data structure
      *
      * \return Pointer to user data structure of the previous node or *null if
      *         there are no more elements before the passed one.
      */
     P tree_rb_string_previous...
     P                 B                   export
     D                 PI              *
     D   nodePtr                       *   const
      *
     D node            DS                  likeds(tree_rb_node_string_t)
     D                                     based(nodePtr)
     D embeddedPtr     S               *
      /free
       embeddedPtr = rbtree_previous(%addr(node.node));
       return tree_rb_string_container_of(embeddedPtr);
      /end-free
     P                 E


     /**
      * \brief Contains key
      *
      * Checks if the tree contains an element with the passed key.
      *
      * \param Pointer to tree
      * \param Key
      *
      * \return *on = tree contains key <br>
      *         *off = tree does not contain key
      */
     P tree_rb_string_containsKey...
     P                 B                   export
     D                 PI              N
     D   tree                          *   const
     D   pKey                     65535A   const varying
      *
     D key             S          65535A   varying
     D header          DS                  likeds(tree_header_t) based(tree)
     D node            DS                  likeds(tree_rb_node_string_t)
     D embeddedPtr     S               *
      /free
       key = pKey;
       
       clear node;
       
       node.keyLength = %len(key);
       node.key = %addr(key : *DATA);
       
       embeddedPtr = rbtree_lookup(%addr(node.node) : header.tree);
       
       return (embeddedPtr <> *null);
      /end-free
     P                 E


     /**
      * \brief Clear tree
      *
      * Removes all nodes from the tree recursively from top to bottom. All
      * memory allocated to the nodes will be freed.
      *
      * \param Pointer to tree
      *
      * \info This procedure traverses the whole tree to complete. The tree
      *       will also rebalance itself on every remove operation. This might
      *       be costly on huge trees. It might be simpler to build a whole new
      *       tree.
      */
     P tree_rb_string_clear...
     P                 B                   export
     D                 PI
     D   tree                          *   const
      *
     D header          DS                  likeds(tree_header_t) based(tree)
     D tree_internal   DS                  likeds(rbtree_t) based(header.tree)
      /free
       dow (tree_internal.root <> *null);
         removeNodesRecursively(tree : tree_internal.root);
       enddo;
      /end-free
     P                 E


     /**
      * \brief Remove node
      *
      * Removes a node from the tree. All allocated resources to the node will
      * be freed.
      *
      * \param Pointer to tree
      * \param Pointer to user data structure (node)
      */
     P tree_rb_string_removeByNode...
     P                 B                   export
     D                 PI
     D   tree                          *   const
     D   nodePtr                       * 
      *
     D header          DS                  likeds(tree_header_t) based(tree)
     D node            DS                  likeds(tree_rb_node_string_t)
     D                                     based(nodePtr)
      /free
       rbtree_remove(%addr(node.node) : header.tree);
       
       if (header.deallocate);
         dealloc node.value;
         dealloc nodePtr;
       endif;
      /end-free
     P                 E


     /**
      * \brief Remove node by key
      *
      * Removes a node from the tree. All allocated resources to the node will
      * be freed. The procedure returns normally if the key does not exist in
      * the tree.
      *
      * \param Pointer to tree
      * \param Key
      */
     P tree_rb_string_remove...
     P                 B                   export
     D                 PI
     D   tree                          *   const
     D   pKey                     65535A   const varying
      *
     D key             S          65535A   varying
     D header          DS                  likeds(tree_header_t) based(tree)
     D node            DS                  likeds(tree_rb_node_string_t)
     D embeddedPtr     S               *
     D userNode        DS                  likeds(tree_rb_node_string_t)
     D                                     based(userNodePtr)
      /free
       key = pKey;
       
       clear node;

       node.keyLength = %len(key);
       node.key = %addr(key : *DATA);
       
       embeddedPtr = rbtree_lookup(%addr(node.node) : header.tree);
       
       if (embeddedPtr <> *null);
         userNodePtr = tree_rb_string_container_of(embeddedPtr);
         rbtree_remove(embeddedPtr : header.tree);

         if (header.deallocate);
           dealloc userNode.value;
           dealloc userNodePtr;
         endif;
       endif;
      /end-free
     P                 E


     /**
      * \brief String compare function
      *
      * This procedure is used for a tree with integer keys. It can be used
      * on the tree creation procedure like this:
      * <code>tree_rb_create(%paddr('tree_rb_string_compare'));</code>
      *
      * \param Embedded node pointer A
      * \param Embedded node pointer B
      *
      * \return less than 0 = key A is less than key B <br>
      *         0 = key A and B are equal <br>
      *         greater than 0 = key A is greater than key B
      */
     P tree_rb_string_compare...
     P                 B                   export
     D                 PI            10I 0
     D  a                              *   value
     D  b                              *   value
      *
     D nodeA           DS                  likeds(tree_rb_node_string_t)
     D                                     based(nodeAPtr)
     D nodeB           DS                  likeds(tree_rb_node_string_t)
     D                                     based(nodeBPtr)
     D offsets         DS                  likeds(tree_rb_node_string_t)
     D nodeOffset      S             10I 0
     D result          S             10I 0
     D minLength       S             10I 0
      /free
       nodeOffset = %addr(offsets.node) - %addr(offsets);
       nodeAPtr = a - nodeOffset;
       nodeBPtr = b - nodeOffset;
 
       if (nodeA.keyLength < nodeB.keyLength);
         minLength = nodeA.keyLength;
       else;
         minLength = nodeB.keyLength;
       endif;
 
       result = strncmp(nodeA.key : nodeB.key : minLength);
       if (result = 0);
         return nodeA.keyLength - nodeB.keyLength;
       else;
         return result;
       endif;
      /end-free
     P                 E


     /**
      * \brief String compare function (ignores case)
      *
      * This procedure is used for a tree with string keys. It can be used
      * on the tree creation procedure like this:
      * <code>tree_rb_create(%paddr('tree_rb_string_compare'));</code>
      *
      * \param Embedded node pointer A
      * \param Embedded node pointer B
      *
      * \return less than 0 = key A is less than key B <br>
      *         0 = key A and B are equal <br>
      *         greater than 0 = key A is greater than key B
      */
     P tree_rb_string_compare_ignore_case...
     P                 B                   export
     D                 PI            10I 0
     D  a                              *   value
     D  b                              *   value
      *
     D nodeA           DS                  likeds(tree_rb_node_string_t)
     D                                     based(nodeAPtr)
     D nodeB           DS                  likeds(tree_rb_node_string_t)
     D                                     based(nodeBPtr)
     D offsets         DS                  likeds(tree_rb_node_string_t)
     D nodeOffset      S             10I 0
     D result          S             10I 0
     D minLength       S             10I 0
      /free
       nodeOffset = %addr(offsets.node) - %addr(offsets);
       nodeAPtr = a - nodeOffset;
       nodeBPtr = b - nodeOffset;
 
       if (nodeA.keyLength < nodeB.keyLength);
         minLength = nodeA.keyLength;
       else;
         minLength = nodeB.keyLength;
       endif;
 
       result = strncasecmp(nodeA.key : nodeB.key : minLength);
       if (result = 0);
         return nodeA.keyLength - nodeB.keyLength;
       else;
         return result;
       endif;
      /end-free
     P                 E


     /**
      * \brief Return pointer to user data structure
      *
      * Returns a pointer to the user data structure of a node relative to the
      * passed embedded data structure pointer.
      *
      * \param Pointer to embedded data structure
      *
      * \return Pointer to user data structure or *null if input parameter is
      *         also *null
      */
     P tree_rb_string_container_of...
     P                 B                   export
     D                 PI              *
     D   eNode                         *   value
      *
     D offsets         DS                  likeds(tree_rb_node_string_t)
     D nodeOffset      S             10I 0
      /free
       if (eNode = *null);
         return *null;
       else;
         nodeOffset = %addr(offsets.node) - %addr(offsets);
         return eNode - nodeOffset;
       endif;
      /end-free
     P                 E


     /**
      * \brief Dispose tree
      *
      * Frees all allocated resources of the tree.
      *
      * \param Pointer to tree
      */
     P tree_rb_string_finalize...
     P                 B                   export
     D                 PI
     D  tree                           *
      *
     D header          DS                  likeds(tree_header_t) based(tree)
      /free
       if (tree <> *null);
         tree_rb_string_clear(tree);
         dealloc header.tree;
         dealloc(n) tree;
       endif;
      /end-free
     P                 E


     P removeNodesRecursively...
     P                 B
     D                 PI
     D   tree                          *   const
     D   nodePtr                       *
      *
     D header          DS                  likeds(tree_header_t) based(tree)
     D userNode        DS                  likeds(tree_rb_node_string_t)
     D                                     based(userNodePtr)
     D node            DS                  likeds(rbnode_t) based(nodePtr)
      /free
       if (node.left <> *null);
         removeNodesRecursively(tree : node.left);
       endif;

       if (node.right <> *null);
         removeNodesRecursively(tree : node.right);
       endif;

       userNodePtr = tree_rb_string_container_of(nodePtr);
       rbtree_remove(nodePtr : header.tree);

       // dealloc memory
       if (header.deallocate);
         dealloc userNode.value;
         dealloc(n) userNodePtr;
       endif;
      /end-free
     P                 E
